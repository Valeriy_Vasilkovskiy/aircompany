package main;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        double[] arrRange = new double[3];
        double[] arrFuel = new double[3];
        double[] arrAllCapacity = new double[3];
        double[] arrAllLoadCapacity = new double[3];
        System.out.println("MAY");
        MAY may = new MAY(220, 30, 20000, 300);
        arrRange[0] = may.range();
        arrAllCapacity[0] = may.allCapacity();
        arrAllLoadCapacity[0] = may.allLoadCapacity();
        arrFuel[0] = may.findFuel();
        System.out.println("//////////////////////////////////");
        System.out.println("Emirates");
        Emirates emirates = new Emirates(240, 40, 10000, 250);
        arrRange[1] = emirates.range();
        arrAllCapacity[1] = emirates.allCapacity();
        arrAllLoadCapacity[1] = emirates.allLoadCapacity();
        arrFuel[1] = emirates.findFuel();
        System.out.println("//////////////////////////////////");
        System.out.println("Lufthansa");
        Lufthansa lufthansa = new Lufthansa(250, 45, 15000, 350);
        arrRange[2] = lufthansa.range();
        arrAllCapacity[2] = lufthansa.allCapacity();
        arrAllLoadCapacity[2] = lufthansa.allLoadCapacity();
        arrFuel[2] = lufthansa.findFuel();
        System.out.println("//////////////////////////////////");
        Main main = new Main();
        main.allCapacity(arrAllCapacity);
        main.allLoadCapacity(arrAllLoadCapacity);
        main.sortByRange(arrRange);
        main.findFuel(arrFuel, 350);
    }

    void sortByRange(double[] arr) {
        for (int i = 0; i != arr.length; i++) {
            for (int j = 0; j != arr.length - 1; j++) {
                if (arr[j + 1] < arr[j]) {
                    double temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }

    void findFuel(double[] arr, double wannaFuel) {
        boolean isComplete = false;
        for (int i = 0; i < arr.length; i++) {
            if (wannaFuel <= arr[i]) {
                switch (i) {
                    case 0:
                        System.out.println("MAY - подходит");
                        break;
                    case 1:
                        System.out.println("Emirates - подходит");
                        break;
                    case 2:
                        System.out.println("Lufthansa - подходит");
                        break;
                }
                isComplete = true;
            }
        }
        if (!isComplete) {
            System.out.println("Нет подходящего");
        }
    }

    void allCapacity(double[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        System.out.println(sum + " вся вместительноть");
    }

    void allLoadCapacity(double[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        System.out.println(sum + " вся грузоподъемность");
    }
}

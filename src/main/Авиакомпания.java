package main;

public abstract class Авиакомпания implements Model {
    private double load_capacity;
    private int capacity;
    private double rangeOfFlight;
    private double fuel;

    public Авиакомпания(double load_capacity, int capacity, double rangeOfFlight, double fuel) {
        this.load_capacity = load_capacity;
        this.capacity = capacity;
        this.rangeOfFlight = rangeOfFlight;
        this.fuel = fuel;
    }

    public double getLoad_capacity() {
        return load_capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public double getRangeOfFlight() {
        return rangeOfFlight;
    }

    public double getFuel() {
        return fuel;
    }
}


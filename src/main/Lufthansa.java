package main;

public class Lufthansa extends Авиакомпания {

    public Lufthansa(double load_capacity, int capacity, double rangeOfFlight, double fuel) {
        super(load_capacity, capacity, rangeOfFlight, fuel);
    }

    @Override
    public double allCapacity() {
        System.out.println(getCapacity()+" - вместимость человек");
        return getCapacity();
    }

    @Override
    public double allLoadCapacity() {
        System.out.println(getLoad_capacity()+" - грузоподъемность");
        return getLoad_capacity();
    }

    @Override
    public double range() {
        System.out.println(getRangeOfFlight()+ " - дальность полёта");
        return getRangeOfFlight();
    }

    @Override
    public double findFuel() {
        System.out.println(getFuel()+" - вместимость бака");
        return getFuel();
    }
}

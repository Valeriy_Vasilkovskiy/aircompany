package main;

public interface Model {
    double allCapacity();
    double allLoadCapacity();
    double findFuel();
    double range();
}
